FROM gcc
RUN apt-get update && apt-get install -y wget cmake libopenblas64-openmp-dev libboost-all-dev build-essential libopenblas64-dev libopenblas-openmp-dev;\
  wget https://github.com/Shark-ML/Shark/archive/v4.0.0.tar.gz;\
  tar -xzf v4.0.0.tar.gz;\
  mv Shark-4.0.0 Shark;\
  mkdir Shark/build/;\
  cd Shark/build;\
  cmake "-DBUILD_EXAMPLES=OFF" ../;\
  make install
